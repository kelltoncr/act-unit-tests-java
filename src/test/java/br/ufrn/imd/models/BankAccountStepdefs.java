package br.ufrn.imd.models;

import br.ufrn.imd.models.BankAccount;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Quando;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class BankAccountStepdefs {

    private BankAccount bankAccountSource;
    private BankAccount bankAccountDestination;

    @Given("um BankAccount com saldo de R$ {double}")
    public void um_bank_account_com_saldo_de_r$(Double double1) {
        //TODO
        bankAccountSource = new BankAccount(123456, 123, double1);
    }

    @When("depositar R$ {double}")
    public void depositar_r$(Double double1) {
        //TODO
        bankAccountSource.deposit(double1);
    }

    @Then("o saldo do BankAccount deve ser R$ {double}")
    public void o_saldo_deve_ser_r$(Double double1) {
        //TODO
        var saldo = bankAccountSource.getBalance();
        assertEquals(double1, saldo);
    }

    @Quando("retirar R$ {double}")
    public void retirarR$(double value) {
        bankAccountSource.withdraw(value);
    }

    @E("um BankAccount de destino com saldo de R$ {double}")
    public void umBankAccountDeDestinoComSaldoDeR$(double value) {
        bankAccountDestination = new BankAccount(123456, 123, value);
    }

    @Quando("tranferir R$ {double} para o BankAccount de destino")
    public void tranferirR$ParaOBankAccountDeDestino(double value) {
        bankAccountSource.transfer(bankAccountDestination, value);
    }

    @E("o saldo do BankAccount de destino deve ser R$ {double}")
    public void oSaldoDoBankAccountDeDestinoDeveSerR$(double value) {
        var saldo = bankAccountDestination.getBalance();
        assertEquals(value, saldo);
    }
}
