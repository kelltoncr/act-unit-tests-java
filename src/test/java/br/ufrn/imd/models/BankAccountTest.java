package br.ufrn.imd.models;

import jdk.jfr.Description;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

public class BankAccountTest {

    private final BankAccountTestFixture fixture = new BankAccountTestFixture();
    private BankAccount bankAccount;

    @BeforeEach
    public void Setup()
    {
        bankAccount = fixture.getNewBankAccount();
    }

    @Test
    @Description("Bank Account should have an account number and agency number")
    public void testBankAccountShouldHaveAccountNumberAndAgency(){
        assertNotNull(bankAccount.getAccountNumber());
        assertNotNull(bankAccount.getAgency());
    }

    @Test
    public void testDepositShouldChangeTheBalance(){
        bankAccount.deposit(100);
        assertEquals(100, bankAccount.getBalance());
    }

    @ParameterizedTest
    @ValueSource(ints = {0, -10, -5, -3, -15})
    public void testDepositShouldNotBeZeroOrNegativeValue(double value){
        assertThrows(IllegalArgumentException.class, () -> bankAccount.deposit(value));
    }

    @ParameterizedTest
    @ValueSource(ints = {101, 200, 250, 300})
    @Description("Testa se há saldo suficiente para o saque")
    public void testWithdrawShouldNotBeLessThanBalance(double value){
        bankAccount.deposit(100);
        assertThrows(IllegalArgumentException.class, () -> bankAccount.withdraw(value));
    }
    @Test
    @Description("Testa a transferência: se há saldo suficiente na conta origem e se o saldo ficou correto na conta destino")
    public void testTransfer(){
        BankAccount beneficiaryAccount = new BankAccount(654321, 321, 0);
        bankAccount.deposit(100);
        bankAccount.transfer( beneficiaryAccount, 90);
        assertEquals(bankAccount.getBalance(),10);
        assertEquals(beneficiaryAccount.getBalance(),90);
    }
}

